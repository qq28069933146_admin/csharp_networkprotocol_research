﻿
namespace MqttnetServerWin
{
    partial class MqttnetServer
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            groupBox2 = new System.Windows.Forms.GroupBox();
            txtIP = new System.Windows.Forms.TextBox();
            label4 = new System.Windows.Forms.Label();
            btn停止 = new System.Windows.Forms.Button();
            groupBox4 = new System.Windows.Forms.GroupBox();
            txt用户名 = new System.Windows.Forms.TextBox();
            label3 = new System.Windows.Forms.Label();
            txt密码 = new System.Windows.Forms.TextBox();
            label2 = new System.Windows.Forms.Label();
            txt端口 = new System.Windows.Forms.TextBox();
            chk检查账户密码 = new System.Windows.Forms.CheckBox();
            label1 = new System.Windows.Forms.Label();
            btn启动 = new System.Windows.Forms.Button();
            groupBox3 = new System.Windows.Forms.GroupBox();
            txt日志 = new System.Windows.Forms.TextBox();
            groupBox2.SuspendLayout();
            groupBox4.SuspendLayout();
            groupBox3.SuspendLayout();
            SuspendLayout();
            // 
            // groupBox2
            // 
            groupBox2.Controls.Add(txtIP);
            groupBox2.Controls.Add(label4);
            groupBox2.Controls.Add(btn停止);
            groupBox2.Controls.Add(groupBox4);
            groupBox2.Controls.Add(txt端口);
            groupBox2.Controls.Add(chk检查账户密码);
            groupBox2.Controls.Add(label1);
            groupBox2.Controls.Add(btn启动);
            groupBox2.Location = new System.Drawing.Point(8, 10);
            groupBox2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            groupBox2.Name = "groupBox2";
            groupBox2.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            groupBox2.Size = new System.Drawing.Size(193, 360);
            groupBox2.TabIndex = 13;
            groupBox2.TabStop = false;
            groupBox2.Text = "服务器配置";
            // 
            // txtIP
            // 
            txtIP.Location = new System.Drawing.Point(71, 22);
            txtIP.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            txtIP.Name = "txtIP";
            txtIP.Size = new System.Drawing.Size(109, 23);
            txtIP.TabIndex = 30;
            txtIP.Text = "127.0.0.1";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(15, 25);
            label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(31, 17);
            label4.TabIndex = 29;
            label4.Text = "IP：";
            // 
            // btn停止
            // 
            btn停止.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            btn停止.Location = new System.Drawing.Point(104, 218);
            btn停止.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            btn停止.Name = "btn停止";
            btn停止.Size = new System.Drawing.Size(76, 28);
            btn停止.TabIndex = 28;
            btn停止.Text = "停止";
            btn停止.UseVisualStyleBackColor = true;
            btn停止.Click += btn停止_Click;
            // 
            // groupBox4
            // 
            groupBox4.Controls.Add(txt用户名);
            groupBox4.Controls.Add(label3);
            groupBox4.Controls.Add(txt密码);
            groupBox4.Controls.Add(label2);
            groupBox4.Location = new System.Drawing.Point(5, 85);
            groupBox4.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            groupBox4.Name = "groupBox4";
            groupBox4.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            groupBox4.Size = new System.Drawing.Size(183, 94);
            groupBox4.TabIndex = 27;
            groupBox4.TabStop = false;
            groupBox4.Text = "校验";
            // 
            // txt用户名
            // 
            txt用户名.Location = new System.Drawing.Point(66, 27);
            txt用户名.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            txt用户名.Name = "txt用户名";
            txt用户名.Size = new System.Drawing.Size(109, 23);
            txt用户名.TabIndex = 29;
            txt用户名.Text = "Admin";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(9, 60);
            label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(56, 17);
            label3.TabIndex = 27;
            label3.Text = "密   码：";
            // 
            // txt密码
            // 
            txt密码.Location = new System.Drawing.Point(66, 58);
            txt密码.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            txt密码.Name = "txt密码";
            txt密码.Size = new System.Drawing.Size(109, 23);
            txt密码.TabIndex = 30;
            txt密码.Text = "Admin123";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(9, 27);
            label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(56, 17);
            label2.TabIndex = 28;
            label2.Text = "用户名：";
            // 
            // txt端口
            // 
            txt端口.Location = new System.Drawing.Point(71, 55);
            txt端口.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            txt端口.Name = "txt端口";
            txt端口.Size = new System.Drawing.Size(109, 23);
            txt端口.TabIndex = 20;
            txt端口.Text = "8085";
            txt端口.KeyPress += txt端口_KeyPress;
            // 
            // chk检查账户密码
            // 
            chk检查账户密码.AutoSize = true;
            chk检查账户密码.Enabled = false;
            chk检查账户密码.Location = new System.Drawing.Point(17, 184);
            chk检查账户密码.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            chk检查账户密码.Name = "chk检查账户密码";
            chk检查账户密码.Size = new System.Drawing.Size(99, 21);
            chk检查账户密码.TabIndex = 21;
            chk检查账户密码.Text = "检查账户密码";
            chk检查账户密码.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(15, 58);
            label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(44, 17);
            label1.TabIndex = 19;
            label1.Text = "端口：";
            // 
            // btn启动
            // 
            btn启动.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            btn启动.Location = new System.Drawing.Point(13, 218);
            btn启动.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            btn启动.Name = "btn启动";
            btn启动.Size = new System.Drawing.Size(76, 28);
            btn启动.TabIndex = 22;
            btn启动.Text = "启动";
            btn启动.UseVisualStyleBackColor = true;
            btn启动.Click += btn启动_Click;
            // 
            // groupBox3
            // 
            groupBox3.Controls.Add(txt日志);
            groupBox3.Location = new System.Drawing.Point(205, 10);
            groupBox3.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            groupBox3.Name = "groupBox3";
            groupBox3.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            groupBox3.Size = new System.Drawing.Size(535, 360);
            groupBox3.TabIndex = 15;
            groupBox3.TabStop = false;
            groupBox3.Text = "服务器日志";
            // 
            // txt日志
            // 
            txt日志.Location = new System.Drawing.Point(5, 22);
            txt日志.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            txt日志.Multiline = true;
            txt日志.Name = "txt日志";
            txt日志.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            txt日志.Size = new System.Drawing.Size(525, 332);
            txt日志.TabIndex = 24;
            // 
            // MqttnetServer
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(748, 378);
            Controls.Add(groupBox3);
            Controls.Add(groupBox2);
            Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            Name = "MqttnetServer";
            Text = "MQTTnet服务器WinForm";
            Load += MqttnetServer_Load;
            groupBox2.ResumeLayout(false);
            groupBox2.PerformLayout();
            groupBox4.ResumeLayout(false);
            groupBox4.PerformLayout();
            groupBox3.ResumeLayout(false);
            groupBox3.PerformLayout();
            ResumeLayout(false);
        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txt端口;
        private System.Windows.Forms.CheckBox chk检查账户密码;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn启动;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txt日志;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txt用户名;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt密码;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn停止;
        private System.Windows.Forms.TextBox txtIP;
        private System.Windows.Forms.Label label4;
    }
}


﻿namespace TCPAndUDPWin
{
    partial class TCPWin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TCPWin));
            panel1 = new System.Windows.Forms.Panel();
            btnOpenCloseThread = new System.Windows.Forms.Button();
            btnSendMsg = new System.Windows.Forms.Button();
            txtMsg = new System.Windows.Forms.TextBox();
            txtIPAddress = new System.Windows.Forms.TextBox();
            label1 = new System.Windows.Forms.Label();
            txtInfo = new System.Windows.Forms.TextBox();
            txtPort = new System.Windows.Forms.TextBox();
            label2 = new System.Windows.Forms.Label();
            panel1.SuspendLayout();
            SuspendLayout();
            // 
            // panel1
            // 
            panel1.Controls.Add(btnOpenCloseThread);
            panel1.Location = new System.Drawing.Point(445, 12);
            panel1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            panel1.Name = "panel1";
            panel1.Size = new System.Drawing.Size(166, 55);
            panel1.TabIndex = 22;
            // 
            // btnOpenCloseThread
            // 
            btnOpenCloseThread.Location = new System.Drawing.Point(16, 12);
            btnOpenCloseThread.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            btnOpenCloseThread.Name = "btnOpenCloseThread";
            btnOpenCloseThread.Size = new System.Drawing.Size(134, 30);
            btnOpenCloseThread.TabIndex = 10;
            btnOpenCloseThread.Text = "开启监听-控制线程";
            btnOpenCloseThread.UseVisualStyleBackColor = true;
            btnOpenCloseThread.Click += btnOpenCloseThread_Click;
            // 
            // btnSendMsg
            // 
            btnSendMsg.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            btnSendMsg.Location = new System.Drawing.Point(331, 19);
            btnSendMsg.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            btnSendMsg.Name = "btnSendMsg";
            btnSendMsg.Size = new System.Drawing.Size(73, 40);
            btnSendMsg.TabIndex = 21;
            btnSendMsg.Text = "发送";
            btnSendMsg.UseVisualStyleBackColor = true;
            btnSendMsg.Click += btnSendMsg_Click;
            // 
            // txtMsg
            // 
            txtMsg.Location = new System.Drawing.Point(70, 44);
            txtMsg.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            txtMsg.Name = "txtMsg";
            txtMsg.Size = new System.Drawing.Size(235, 23);
            txtMsg.TabIndex = 19;
            // 
            // txtIPAddress
            // 
            txtIPAddress.Location = new System.Drawing.Point(70, 15);
            txtIPAddress.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            txtIPAddress.Name = "txtIPAddress";
            txtIPAddress.Size = new System.Drawing.Size(153, 23);
            txtIPAddress.TabIndex = 16;
            txtIPAddress.Text = "127.0.0.1";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(10, 18);
            label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(54, 17);
            label1.TabIndex = 15;
            label1.Text = "TCP地址";
            // 
            // txtInfo
            // 
            txtInfo.Location = new System.Drawing.Point(10, 73);
            txtInfo.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            txtInfo.Multiline = true;
            txtInfo.Name = "txtInfo";
            txtInfo.Size = new System.Drawing.Size(604, 303);
            txtInfo.TabIndex = 23;
            // 
            // txtPort
            // 
            txtPort.Location = new System.Drawing.Point(228, 15);
            txtPort.Name = "txtPort";
            txtPort.Size = new System.Drawing.Size(77, 23);
            txtPort.TabIndex = 24;
            txtPort.Text = "8085";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(11, 47);
            label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(44, 17);
            label2.TabIndex = 25;
            label2.Text = "消息：";
            // 
            // TCPWin
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(622, 382);
            Controls.Add(label2);
            Controls.Add(txtPort);
            Controls.Add(panel1);
            Controls.Add(btnSendMsg);
            Controls.Add(txtMsg);
            Controls.Add(txtIPAddress);
            Controls.Add(label1);
            Controls.Add(txtInfo);
            Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            Name = "TCPWin";
            Text = "TCPService示例";
            Load += TCPWin_Load;
            panel1.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnOpenCloseThread;
        private System.Windows.Forms.Button btnSendMsg;
        private System.Windows.Forms.TextBox txtMsg;
        private System.Windows.Forms.TextBox txtIPAddress;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtInfo;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.Label label2;
    }
}
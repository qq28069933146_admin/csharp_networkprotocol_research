﻿/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：TCP通讯相关的工具类                                                   
*│　作    者：执笔小白                                              
*│　版    本：1.1                                       
*│　创建时间：2023-3-18 10:40:56                            
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: TCPAndUDPWin                               
*│　类    名：TCPHelper                                     
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TCPAndUDPWin
{
    /// <summary>
    /// TCP通信帮助类
    /// System.Net.Sockets类库
    /// TcpListener与TcpClient
    /// </summary>
    public class TCPHelper
    {
        #region 接收端（侦听端）
        /// <summary>
        /// 侦听来自 TCP 网络客户端的连接
        /// </summary>
        TcpListener _server = null;

        /// <summary>
        /// 取消线程的标识
        /// </summary>
        private CancellationTokenSource cts;

        #region 属性
        /// <summary>
        /// 获取或设置一个 Boolean 值，该值指定 TcpListener 是否只允许一个基础套接字来侦听特定端口。
        /// </summary>
        public bool TcpListener_ExclusiveAddressUse => _server.ExclusiveAddressUse;

        /// <summary>
        /// 获取当前 EndPoint 的基础 TcpListener
        /// </summary>
        public EndPoint TcpListener_LocalEndpoint => _server.LocalEndpoint;

        /// <summary>
        /// 获取基础网络 Socket
        /// </summary>
        public Socket TcpListener_Server => _server.Server;

        #endregion 属性

        /// <summary>
        /// 创建侦听
        /// </summary>
        /// <param name="localAddr">地址</param>
        /// <param name="port">端口</param>
        /// <returns></returns>
        public TcpListener TcpListener_Start(IPAddress localAddr, int port)
        {
            _server = new TcpListener(localAddr, port);
            return _server;
        }

        /// <summary>
        /// 创建并开启TCP侦听
        /// </summary>
        /// <param name="localAddr">地址</param>
        /// <param name="port">端口</param>
        /// <param name="callback">委托方法</param>
        /// <returns></returns>
        public void TcpListener_Start(IPAddress localAddr, int port, Action<ResultData_TCP>? callback)
        {
            try
            {
                if (localAddr != null)
                {
                    _server = new TcpListener(localAddr, port);
                }
                else
                {
                    _server = new TcpListener(port);
                }

                _server.Start();

                cts = new CancellationTokenSource();
                Task.Run(() => ThreadCode(cts.Token, callback));
            }
            catch (SocketException e)
            {
                Console.WriteLine("Socket异常，错误信息: {0}", e.HResult + "_" + e.Message);

                ResultData_TCP state = new ResultData_TCP()
                {
                    ResultCode = -1,
                    ResultMsg = string.Concat("SocketException：", e.HResult, "，", e.Message)
                };
                callback?.Invoke(state);
            }
            catch (Exception ex)
            {
                Console.WriteLine("接收出错，错误信息: {0}", ex.HResult + "_" + ex.Message);

                ResultData_TCP state = new ResultData_TCP()
                {
                    ResultCode = -1,
                    ResultMsg = string.Concat("出错，错误内容: ", ex.HResult, "，", ex.Message)
                };
                callback?.Invoke(state);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 监听
        /// </summary>
        /// <param name="token">线程取消的标识</param>
        /// <param name="callback">委托</param>
        private void ThreadCode(CancellationToken token, Action<ResultData_TCP>? callback)
        {
            while (!token.IsCancellationRequested)
            {
                try
                {
                    TcpClient client = _server.AcceptTcpClient();  // 接受一个Client
                    NetworkStream stream = client.GetStream();           // 获取网络流

                    // 接收信息
                    byte[] buffer = new byte[client.ReceiveBufferSize];  // 存储接收到的流
                    stream.Read(buffer, 0, buffer.Length);               // 读取网络流中的数据
                    string dataStr = System.Text.Encoding.ASCII.GetString(buffer, 0, buffer.Length);
                    Console.WriteLine("Received: {0}", dataStr);

                    // 发回回复
                    byte[] msg = System.Text.Encoding.ASCII.GetBytes(dataStr);
                    stream.Write(msg, 0, msg.Length);
                    string msgStr = System.Text.Encoding.ASCII.GetString(msg, 0, msg.Length);
                    Console.WriteLine("Sent: {0}", msgStr);

                    stream.Close();  // 关闭流
                    client.Close();  // 关闭Client

                    ResultData_TCP state = new ResultData_TCP()
                    {
                        ResultCode = 1,
                        ResultMsg = "接收成功！",
                        ResultObject1 = dataStr,  // 接收的信息
                        ResultObject2 = msgStr    // 回复的信息
                    };
                    callback?.Invoke(state);
                }
                catch (SocketException e)
                {
                    Console.WriteLine("Socket异常，错误信息: {0}", e.HResult + "_" + e.Message);

                    ResultData_TCP state = new ResultData_TCP()
                    {
                        ResultCode = -1,
                        ResultMsg = string.Concat("SocketException：", e.HResult, "，", e.Message)
                    };
                    callback?.Invoke(state);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("接收出错，错误信息: {0}", ex.HResult + "_" + ex.Message);

                    ResultData_TCP state = new ResultData_TCP()
                    {
                        ResultCode = -1,
                        ResultMsg = string.Concat("出错，错误内容: ", ex.HResult, "，", ex.Message)
                    };
                    callback?.Invoke(state);
                }
            }
            _server.Stop();  // 关闭
        }

        /// <summary>
        /// TCP监听停止
        /// </summary>
        public void TcpListener_Stop()
        {
            cts.Cancel();
        }

        /// <summary>
        /// 确定是否有挂起的连接请求
        /// </summary>
        /// <returns></returns>
        public bool TcpListener_Pending() => _server.Pending();

        /// <summary>
        /// 启用或禁用针对 TcpListener 实例的网络地址转换(NAT) 遍历。
        /// </summary>
        /// <param name="allowed">启用或禁用</param>
        public void TcpListener_AllowNatTraversal(bool allowed) => _server.AllowNatTraversal(allowed);

        /// <summary>
        /// 接受挂起的连接请求
        /// </summary>
        /// <returns></returns>
        public Socket TcpListener_AcceptSocket() => _server.AcceptSocket();

        /// <summary>
        /// 接受挂起的连接请求以作为异步操作。
        /// </summary>
        /// <returns></returns>
        public Task<Socket> TcpListener_AcceptSocketAsync() => _server.AcceptSocketAsync();

        /// <summary>
        /// 接受挂起的连接请求。
        /// </summary>
        /// <returns></returns>
        public TcpClient TcpListener_AcceptTcpClient() => _server.AcceptTcpClient();

        /// <summary>
        /// 接受挂起的连接请求以作为异步操作。
        /// </summary>
        /// <returns></returns>
        public Task<TcpClient> TcpListener_AcceptTcpClientAsync() => _server.AcceptTcpClientAsync();

        /// <summary>
        /// 开始一个异步操作来接受一个传入的连接尝试。
        /// </summary>
        /// <returns></returns>
        public IAsyncResult TcpListener_BeginAcceptSocket(AsyncCallback? callback, object? state) => _server.BeginAcceptSocket(callback, state);

        /// <summary>
        /// 异步接受传入的连接尝试，并创建新的 Socket 来处理远程主机通信。
        /// </summary>
        /// <param name="asyncResult"></param>
        /// <returns></returns>
        public Socket TcpListener_EndAcceptSocket(IAsyncResult asyncResult) => _server.EndAcceptSocket(asyncResult);

        /// <summary>
        /// 开始一个异步操作来接受一个传入的连接尝试。
        /// </summary>
        /// <param name="callback"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public IAsyncResult TcpListener_BeginAcceptTcpClient(AsyncCallback? callback, object? state) => _server.BeginAcceptTcpClient(callback, state);

        /// <summary>
        /// 异步接受传入的连接尝试，并创建新的 TcpClient 来处理远程主机通信。
        /// </summary>
        /// <param name="asyncResult"></param>
        /// <returns></returns>
        public TcpClient TcpListener_EndAcceptTcpClient(IAsyncResult asyncResult) => _server.EndAcceptTcpClient(asyncResult);
        #endregion 接收端（侦听端）

        #region 发送端（客户端）
        /// <summary>
        /// 为 TCP 网络服务提供客户端连接
        /// </summary>
        TcpClient _tcpClient = new TcpClient();

        #region 属性
        /// <summary>
        /// 是否已建立连接
        /// </summary>
        public bool TcpClient_Connected => _tcpClient.Connected;

        /// <summary>
        /// 获取已经从网络接收且可供读取的数据量
        /// </summary>
        public int TcpClient_Available => _tcpClient.Available;

        /// <summary>
        /// 获取当前连接用对象
        /// </summary>
        /// <returns></returns>
        public Socket TcpClient_Client() => _tcpClient.Client;

        /// <summary>
        /// 指定 TcpClient 是否只允许一个客户端使用端口
        /// </summary>
        public bool TcpClient_ExclusiveAddressUse => _tcpClient.ExclusiveAddressUse;

        /// <summary>
        /// 获取或设置有关关联的套接字的延迟状态的信息。
        /// </summary>
        public LingerOption? TcpClient_LingerState => _tcpClient.LingerState;

        /// <summary>
        /// 获取或设置一个值，该值在发送或接收缓冲区未满时禁用延迟。
        /// </summary>
        public bool TcpClient_NoDelay => _tcpClient.NoDelay;

        /// <summary>
        /// 获取或设置接收缓冲区的大小。
        /// </summary>
        public int TcpClient_ReceiveBufferSize => _tcpClient.ReceiveBufferSize;

        /// <summary>
        /// 获取或设置在初始化一个读取操作以后 TcpClient 等待接收数据的时间量。
        /// </summary>
        public int TcpClient_ReceiveTimeout => _tcpClient.ReceiveTimeout;

        /// <summary>
        /// 获取或设置发送缓冲区的大小。
        /// </summary>
        public int TcpClient_SendBufferSize => _tcpClient.SendBufferSize;

        /// <summary>
        /// 获取或设置 TcpClient 等待发送操作成功完成的时间量。
        /// </summary>
        public int TcpClient_SendTimeout => _tcpClient.SendTimeout;
        #endregion 属性

        /// <summary>
        /// 连接
        /// </summary>
        /// <param name="hostname">Ip</param>
        /// <param name="port">端口</param>
        /// <returns></returns>
        public bool TcpClient_Connect(string hostname, int port)
        {
            _tcpClient = new TcpClient(hostname, port);
            return _tcpClient.Connected;
        }

        /// <summary>
        /// 连接
        /// </summary>
        /// <param name="hostname">Ip</param>
        /// <param name="port">端口</param>
        /// <returns></returns>
        public TcpClient TcpClient_Connect2(string hostname, int port)
        {
            return new TcpClient(hostname, port);
        }

        /// <summary>
        /// 连接_异步
        /// </summary>
        /// <param name="hostname">Ip</param>
        /// <param name="port">端口</param>
        /// <returns></returns>
        public Task TcpClient_ConnectAsync(string hostname, int port) => _tcpClient.ConnectAsync(hostname, port);

        /// <summary>
        /// 连接_异步 令牌
        /// </summary>
        /// <param name="hostname">Ip</param>
        /// <param name="port">端口</param>
        /// <param name="cancellationToken">令牌</param>
        /// <returns></returns>
        public ValueTask TcpClient_ConnectAsync(string hostname, int port, CancellationToken cancellationToken) => _tcpClient.ConnectAsync(hostname, port, cancellationToken);

        /// <summary>
        /// 关闭连接
        /// </summary>
        public void TcpClient_Close() => _tcpClient.Close();

        /// <summary>
        /// 开始一个对远程主机连接_异步
        /// </summary>
        /// <param name="host">IP</param>
        /// <param name="port">端口</param>
        /// <param name="requestCallback">回调方法</param>
        /// <param name="state">当操作完成时，传递给 requestCallback 委托的对象</param>
        /// <returns></returns>
        public IAsyncResult TcpClient_BeginConnect(string host, int port, AsyncCallback? requestCallback, object? state) => _tcpClient.BeginConnect(host, port, requestCallback, state);

        /// <summary>
        /// 结束异步连接
        /// </summary>
        /// <param name="asyncResult"></param>
        public void TcpClient_EndConnect(IAsyncResult asyncResult) => _tcpClient.EndConnect(asyncResult);

        /// <summary>
        /// 返回用于向远程主机读写数据的流。
        /// </summary>
        /// <returns></returns>
        public NetworkStream TcpClient_GetStream() => _tcpClient.GetStream();

        #region 接收与发送消息-未做粘包和拆包处理
        /// <summary>
        /// 发送消息-未做粘包和拆包处理（一般不用）
        /// </summary>
        /// <param name="msg">数据流</param>
        /// <returns></returns>
        public ResultData_TCP SendMsg_Nohandle(string msg)
        {
            ResultData_TCP resultData_TCP = new ResultData_TCP();

            try
            {
                byte[] data = System.Text.Encoding.ASCII.GetBytes(msg);

                NetworkStream stream = _tcpClient.GetStream();  // 获取当前数据流
                stream.Write(data, 0, data.Length);  // 写入
                stream.Flush();
                stream.Close();  // 关闭流

                resultData_TCP = new ResultData_TCP()
                {
                    ResultCode = 1,
                    ResultMsg = "发送成功！"
                };
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine("ArgumentNullException: {0}", e);
                resultData_TCP = new ResultData_TCP()
                {
                    ResultCode = -1,
                    ResultMsg = string.Concat("参数无效；错误内容: ", e.HResult, "，", e.Message)
                };
            }  // 参数无效（当将 null 引用传递到不接受其作为有效参数的方法时引发的异常。）
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
                resultData_TCP = new ResultData_TCP()
                {
                    ResultCode = -1,
                    ResultMsg = string.Concat("网络异常，套接字错误；错误内容: ", e.HResult, "，", e.Message)
                };
            }  // 套接字错误（发生套接字错误时引发的异常。）
            catch (Exception ex)
            {
                resultData_TCP = new ResultData_TCP()
                {
                    ResultCode = -1,
                    ResultMsg = string.Concat("发生错误；错误内容: ", ex.HResult, "，", ex.Message),
                };
            }  // 其他错误信息
            return resultData_TCP;
        }

        /// <summary>
        /// 接收一次消息-未做粘包和拆包处理
        /// 注：该方法不会使用；一般TCP消息使用TcpListener做接收。
        /// </summary>
        /// <returns></returns>
        public ResultData_TCP ReceiveMsg1_Nohandle()
        {
            ResultData_TCP resultData_TCP = new ResultData_TCP();

            try
            {
                byte[] data = new byte[1024];  // 接收的缓存
                string responseData = string.Empty;  // 接收用对象

                NetworkStream stream = _tcpClient.GetStream();  // 获取流
                int bytes = stream.Read(data, 0, data.Length);  // 读取
                responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                stream.Flush();
                stream.Close();  // 关闭流

                resultData_TCP = new ResultData_TCP()
                {
                    ResultCode = 1,
                    ResultMsg = "接收成功！",
                    ResultObject1 = responseData
                };
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine("ArgumentNullException: {0}", e);
                resultData_TCP = new ResultData_TCP()
                {
                    ResultCode = -1,
                    ResultMsg = string.Concat("参数无效；错误内容: ", e.HResult, "，", e.Message)
                };
            }  // 参数无效（当将 null 引用传递到不接受其作为有效参数的方法时引发的异常。）
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
                resultData_TCP = new ResultData_TCP()
                {
                    ResultCode = -1,
                    ResultMsg = string.Concat("网络异常，套接字错误；错误内容: ", e.HResult, "，", e.Message)
                };
            }  // 套接字错误（发生套接字错误时引发的异常。）
            catch (Exception ex)
            {
                resultData_TCP = new ResultData_TCP()
                {
                    ResultCode = -1,
                    ResultMsg = string.Concat("发生错误；错误内容: ", ex.HResult, "，", ex.Message),
                };
            }  // 其他错误信息

            return resultData_TCP;
        }

        /// <summary>
        /// 发送消息-未做粘包和拆包处理
        /// </summary>
        /// <param name="hostname">Ip</param>
        /// <param name="port">端口</param>
        /// <param name="msg">数据流</param>
        /// <returns></returns>
        public ResultData_TCP ConnectAndSendMsg_Nohandle(string hostname, int port, string msg)
        {
            ResultData_TCP resultData_TCP = new ResultData_TCP();

            try
            {
                _tcpClient = new TcpClient(hostname, port);

                byte[] data = System.Text.Encoding.ASCII.GetBytes(msg);

                NetworkStream stream = _tcpClient.GetStream();  // 获取当前数据流
                stream.Write(data, 0, data.Length);  // 写入
                stream.Flush();
                stream.Close();  // 关闭流

                resultData_TCP = new ResultData_TCP()
                {
                    ResultCode = 1,
                    ResultMsg = "发送成功！"
                };
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine("ArgumentNullException: {0}", e);
                resultData_TCP = new ResultData_TCP()
                {
                    ResultCode = -1,
                    ResultMsg = string.Concat("参数无效；错误内容: ", e.HResult, "，", e.Message)
                };
            }  // 参数无效（当将 null 引用传递到不接受其作为有效参数的方法时引发的异常。）
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
                resultData_TCP = new ResultData_TCP()
                {
                    ResultCode = -1,
                    ResultMsg = string.Concat("网络异常，套接字错误；错误内容: ", e.HResult, "，", e.Message)
                };
            }  // 套接字错误（发生套接字错误时引发的异常。）
            catch (Exception ex)
            {
                resultData_TCP = new ResultData_TCP()
                {
                    ResultCode = -1,
                    ResultMsg = string.Concat("发生错误；错误内容: ", ex.HResult, "，", ex.Message),
                };
            }  // 其他错误信息
            return resultData_TCP;
        }
        #endregion 接收与发送消息-未做粘包和拆包处理
        #endregion 发送端（客户端）
    }

    /// <summary>
    /// 信息载体
    /// </summary>
    public class ResultData_TCP
    {
        /// <summary>
        /// 结果Code
        /// 正常1，其他为异常；0不作为回复结果
        /// </summary>
        public int ResultCode { get; set; } = 0;

        /// <summary>
        /// 结果信息
        /// </summary>
        public string ResultMsg { get; set; } = string.Empty;

        /// <summary>
        /// 扩展1
        /// </summary>
        public object? ResultObject1 { get; set; } = string.Empty;

        /// <summary>
        /// 扩展2
        /// </summary>
        public object? ResultObject2 { get; set; } = string.Empty;
    }
}
﻿
namespace WinSocketServer
{
    partial class WinSocketServer1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            btnModify = new System.Windows.Forms.Button();
            label1 = new System.Windows.Forms.Label();
            txtIPAddress = new System.Windows.Forms.TextBox();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            txtAvalue = new System.Windows.Forms.TextBox();
            txtBvalue = new System.Windows.Forms.TextBox();
            btnBroadcast = new System.Windows.Forms.Button();
            panel1 = new System.Windows.Forms.Panel();
            btnClose = new System.Windows.Forms.Button();
            btnOpenServer = new System.Windows.Forms.Button();
            txtInfo = new System.Windows.Forms.TextBox();
            btnShowCon = new System.Windows.Forms.Button();
            lblListen = new System.Windows.Forms.Label();
            panel1.SuspendLayout();
            SuspendLayout();
            // 
            // btnModify
            // 
            btnModify.Location = new System.Drawing.Point(271, 10);
            btnModify.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            btnModify.Name = "btnModify";
            btnModify.Size = new System.Drawing.Size(73, 25);
            btnModify.TabIndex = 0;
            btnModify.Text = "修改";
            btnModify.UseVisualStyleBackColor = true;
            btnModify.Click += btnModify_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(9, 14);
            label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(56, 17);
            label1.TabIndex = 1;
            label1.Text = "监听地址";
            // 
            // txtIPAddress
            // 
            txtIPAddress.Enabled = false;
            txtIPAddress.Location = new System.Drawing.Point(68, 11);
            txtIPAddress.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            txtIPAddress.Name = "txtIPAddress";
            txtIPAddress.Size = new System.Drawing.Size(200, 23);
            txtIPAddress.TabIndex = 2;
            txtIPAddress.Text = "http://127.0.0.1:8080/";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(9, 48);
            label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(27, 17);
            label2.TabIndex = 3;
            label2.Text = "a：";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(9, 78);
            label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(28, 17);
            label3.TabIndex = 4;
            label3.Text = "b：";
            // 
            // txtAvalue
            // 
            txtAvalue.Location = new System.Drawing.Point(68, 45);
            txtAvalue.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            txtAvalue.Name = "txtAvalue";
            txtAvalue.Size = new System.Drawing.Size(161, 23);
            txtAvalue.TabIndex = 5;
            // 
            // txtBvalue
            // 
            txtBvalue.Location = new System.Drawing.Point(68, 76);
            txtBvalue.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            txtBvalue.Name = "txtBvalue";
            txtBvalue.Size = new System.Drawing.Size(161, 23);
            txtBvalue.TabIndex = 6;
            // 
            // btnBroadcast
            // 
            btnBroadcast.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            btnBroadcast.Location = new System.Drawing.Point(244, 48);
            btnBroadcast.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            btnBroadcast.Name = "btnBroadcast";
            btnBroadcast.Size = new System.Drawing.Size(100, 48);
            btnBroadcast.TabIndex = 7;
            btnBroadcast.Text = "广播";
            btnBroadcast.UseVisualStyleBackColor = true;
            btnBroadcast.Click += btnBroadcast_Click;
            // 
            // panel1
            // 
            panel1.Controls.Add(btnClose);
            panel1.Controls.Add(btnOpenServer);
            panel1.Location = new System.Drawing.Point(368, 3);
            panel1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            panel1.Name = "panel1";
            panel1.Size = new System.Drawing.Size(110, 96);
            panel1.TabIndex = 10;
            // 
            // btnClose
            // 
            btnClose.Location = new System.Drawing.Point(19, 60);
            btnClose.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            btnClose.Name = "btnClose";
            btnClose.Size = new System.Drawing.Size(73, 25);
            btnClose.TabIndex = 11;
            btnClose.Text = "关闭监听";
            btnClose.UseVisualStyleBackColor = true;
            btnClose.Click += btnClose_Click;
            // 
            // btnOpenServer
            // 
            btnOpenServer.Location = new System.Drawing.Point(19, 11);
            btnOpenServer.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            btnOpenServer.Name = "btnOpenServer";
            btnOpenServer.Size = new System.Drawing.Size(73, 25);
            btnOpenServer.TabIndex = 10;
            btnOpenServer.Text = "开启监听";
            btnOpenServer.UseVisualStyleBackColor = true;
            btnOpenServer.Click += btnOpenServer_Click;
            // 
            // txtInfo
            // 
            txtInfo.Location = new System.Drawing.Point(9, 121);
            txtInfo.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            txtInfo.Multiline = true;
            txtInfo.Name = "txtInfo";
            txtInfo.Size = new System.Drawing.Size(604, 252);
            txtInfo.TabIndex = 11;
            // 
            // btnShowCon
            // 
            btnShowCon.Location = new System.Drawing.Point(510, 56);
            btnShowCon.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            btnShowCon.Name = "btnShowCon";
            btnShowCon.Size = new System.Drawing.Size(92, 39);
            btnShowCon.TabIndex = 12;
            btnShowCon.Text = "显示当前连接";
            btnShowCon.UseVisualStyleBackColor = true;
            btnShowCon.Click += btnShowCon_Click;
            // 
            // lblListen
            // 
            lblListen.AutoSize = true;
            lblListen.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            lblListen.Location = new System.Drawing.Point(510, 18);
            lblListen.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            lblListen.Name = "lblListen";
            lblListen.Size = new System.Drawing.Size(92, 17);
            lblListen.TabIndex = 13;
            lblListen.Text = "服务为关闭状态";
            lblListen.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // WinSocketServer1
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(622, 382);
            Controls.Add(lblListen);
            Controls.Add(btnShowCon);
            Controls.Add(txtInfo);
            Controls.Add(panel1);
            Controls.Add(btnBroadcast);
            Controls.Add(txtBvalue);
            Controls.Add(txtAvalue);
            Controls.Add(label3);
            Controls.Add(label2);
            Controls.Add(txtIPAddress);
            Controls.Add(label1);
            Controls.Add(btnModify);
            Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            Name = "WinSocketServer1";
            Text = "Win_WebSocket服务器";
            Load += WinSockerServer1_Load;
            panel1.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Button btnModify;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtIPAddress;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtAvalue;
        private System.Windows.Forms.TextBox txtBvalue;
        private System.Windows.Forms.Button btnBroadcast;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnOpenServer;
        private System.Windows.Forms.TextBox txtInfo;
        private System.Windows.Forms.Button btnShowCon;
        private System.Windows.Forms.Label lblListen;
    }
}


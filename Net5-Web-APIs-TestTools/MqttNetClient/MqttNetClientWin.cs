﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MqttnetServerWin;

namespace MqttNetClient
{
    public partial class MqttNetClientWin : Form
    {
        #region 变量
        /// <summary>
        /// MQTT帮助类
        /// </summary>
        MQTTHelper _MQTTHelper = new MQTTHelper();
        #endregion 变量

        public MqttNetClientWin()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 启动按钮
        /// </summary>
        private void btn启动_Click(object sender, EventArgs e)
        {
            string iPStr = txtIP.Text.Trim();
            string portStr = txt端口.Text.Trim();
            string uName = txt用户名.Text.Trim();
            string uPwd = txt密码.Text.Trim();

            if (string.IsNullOrEmpty(iPStr))
            {
                MessageBox.Show("IP地址不可为空");
                return;
            }

            if (string.IsNullOrEmpty(portStr))
            {
                MessageBox.Show("端口不可为空");
                return;
            }
            int port = Convert.ToInt32(portStr);
            bool isCredentials = chk检查账户密码.Checked;
            if (isCredentials)  // 是否检测用户名和密码
            {
                if (string.IsNullOrEmpty(uName))
                {
                    MessageBox.Show("用户名不可为空");
                    return;
                }
                if (string.IsNullOrEmpty(uPwd))
                {
                    MessageBox.Show("密码不可为空");
                    return;
                }
            }

            Action<ResultData_MQTT>? callback = ShowLog;
            _MQTTHelper.CreateMQTTClientAndStart(iPStr, port, uName, uPwd, callback);  // 连接MQTT服务器
        }

        /// <summary>
        /// 停止按钮
        /// </summary>
        private void btn停止_Click(object sender, EventArgs e)
        {
            _MQTTHelper.DisconnectAsync_Client();
        }

        /// <summary>
        /// 订阅按钮
        /// </summary>
        private void btn订阅_Click(object sender, EventArgs e)
        {
            string topic = txt主题.Text.Trim();
            if (string.IsNullOrEmpty(topic))
            {
                MessageBox.Show("订阅主题不能为空！");
                return;
            }
            _MQTTHelper.SubscribeAsync_Client(topic);
        }

        /// <summary>
        /// 退订按钮
        /// </summary>
        private void btn退订_Click(object sender, EventArgs e)
        {
            string topic = txt主题.Text.Trim();
            if (string.IsNullOrEmpty(topic))
            {
                MessageBox.Show("退订主题不能为空！");
                return;
            }
            _MQTTHelper.UnsubscribeAsync_Client(topic);
        }

        // 发送按钮
        private void btn发送_Click(object sender, EventArgs e)
        {
            string dTopic = txt当前主题.Text.Trim();  // 主题
            string contentStr = txt内容.Text.Trim();  // 消息
            bool retained = ckbRetained.Checked;      // 是否保留
            if (string.IsNullOrEmpty(dTopic) || dTopic == "未订阅主题")
            {
                MessageBox.Show("请先指定主题!");
                return;
            }
            if (string.IsNullOrEmpty(contentStr))
            {
                MessageBox.Show("发送的消息不可为空!");
                return;
            }

            _MQTTHelper.PublishAsync_Client(dTopic, contentStr, retained);  // 发布信息

            // 清空消息框
            txt内容.Text = "";
        }

        #region 方法
        /// <summary>
        /// 处理逻辑-展示Log
        /// </summary>
        /// <param name="obj"></param>
        private void ShowLog(ResultData_MQTT resultData_MQTT)
        {
            this.Invoke(new Action(() =>
            {
                txt日志.Text += $"\r\n返回结果：{resultData_MQTT.ResultCode}，返回信息：{resultData_MQTT.ResultMsg}";
            }));
        }
        #endregion 方法

        #region 其他
        /// <summary>
        /// 端口只允许输入数字
        /// </summary>
        private void txt端口_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != '\b')//这是允许输入退格键  
            {
                if ((e.KeyChar < '0') || (e.KeyChar > '9'))//这是允许输入0-9数字  
                {
                    e.Handled = true;
                }
            }
        }
        #endregion 其他
    }
}

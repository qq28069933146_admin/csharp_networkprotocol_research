﻿
namespace MqttNetClient
{
    partial class MqttNetClientWin
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            groupBox3 = new System.Windows.Forms.GroupBox();
            txt日志 = new System.Windows.Forms.TextBox();
            groupBox1 = new System.Windows.Forms.GroupBox();
            ckbRetained = new System.Windows.Forms.CheckBox();
            btn发送 = new System.Windows.Forms.Button();
            txt内容 = new System.Windows.Forms.TextBox();
            txt当前主题 = new System.Windows.Forms.TextBox();
            label6 = new System.Windows.Forms.Label();
            groupBox2 = new System.Windows.Forms.GroupBox();
            txt端口 = new System.Windows.Forms.TextBox();
            label5 = new System.Windows.Forms.Label();
            btn停止 = new System.Windows.Forms.Button();
            groupBox4 = new System.Windows.Forms.GroupBox();
            txt用户名 = new System.Windows.Forms.TextBox();
            label3 = new System.Windows.Forms.Label();
            txt密码 = new System.Windows.Forms.TextBox();
            label2 = new System.Windows.Forms.Label();
            txtIP = new System.Windows.Forms.TextBox();
            chk检查账户密码 = new System.Windows.Forms.CheckBox();
            label1 = new System.Windows.Forms.Label();
            btn启动 = new System.Windows.Forms.Button();
            groupBox5 = new System.Windows.Forms.GroupBox();
            btn退订 = new System.Windows.Forms.Button();
            btn订阅 = new System.Windows.Forms.Button();
            label4 = new System.Windows.Forms.Label();
            txt主题 = new System.Windows.Forms.TextBox();
            groupBox3.SuspendLayout();
            groupBox1.SuspendLayout();
            groupBox2.SuspendLayout();
            groupBox4.SuspendLayout();
            groupBox5.SuspendLayout();
            SuspendLayout();
            // 
            // groupBox3
            // 
            groupBox3.Controls.Add(txt日志);
            groupBox3.Location = new System.Drawing.Point(220, 10);
            groupBox3.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            groupBox3.Name = "groupBox3";
            groupBox3.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            groupBox3.Size = new System.Drawing.Size(394, 275);
            groupBox3.TabIndex = 18;
            groupBox3.TabStop = false;
            groupBox3.Text = "MQTT日志";
            // 
            // txt日志
            // 
            txt日志.Location = new System.Drawing.Point(5, 22);
            txt日志.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            txt日志.Multiline = true;
            txt日志.Name = "txt日志";
            txt日志.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            txt日志.Size = new System.Drawing.Size(380, 249);
            txt日志.TabIndex = 24;
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(ckbRetained);
            groupBox1.Controls.Add(btn发送);
            groupBox1.Controls.Add(txt内容);
            groupBox1.Controls.Add(txt当前主题);
            groupBox1.Controls.Add(label6);
            groupBox1.Location = new System.Drawing.Point(220, 291);
            groupBox1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            groupBox1.Name = "groupBox1";
            groupBox1.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            groupBox1.Size = new System.Drawing.Size(394, 82);
            groupBox1.TabIndex = 17;
            groupBox1.TabStop = false;
            groupBox1.Text = "发送消息";
            // 
            // ckbRetained
            // 
            ckbRetained.AutoSize = true;
            ckbRetained.Location = new System.Drawing.Point(310, 24);
            ckbRetained.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            ckbRetained.Name = "ckbRetained";
            ckbRetained.Size = new System.Drawing.Size(75, 21);
            ckbRetained.TabIndex = 22;
            ckbRetained.Text = "是否保留";
            ckbRetained.UseVisualStyleBackColor = true;
            // 
            // btn发送
            // 
            btn发送.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            btn发送.Location = new System.Drawing.Point(344, 53);
            btn发送.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            btn发送.Name = "btn发送";
            btn发送.Size = new System.Drawing.Size(41, 25);
            btn发送.TabIndex = 11;
            btn发送.Text = "发送";
            btn发送.UseVisualStyleBackColor = true;
            btn发送.Click += btn发送_Click;
            // 
            // txt内容
            // 
            txt内容.Location = new System.Drawing.Point(5, 54);
            txt内容.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            txt内容.Name = "txt内容";
            txt内容.Size = new System.Drawing.Size(335, 23);
            txt内容.TabIndex = 10;
            // 
            // txt当前主题
            // 
            txt当前主题.Location = new System.Drawing.Point(121, 22);
            txt当前主题.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            txt当前主题.Name = "txt当前主题";
            txt当前主题.Size = new System.Drawing.Size(160, 23);
            txt当前主题.TabIndex = 9;
            txt当前主题.Text = "未订阅主题";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(5, 25);
            label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(116, 17);
            label6.TabIndex = 8;
            label6.Text = "当前订阅的主题为：";
            // 
            // groupBox2
            // 
            groupBox2.Controls.Add(txt端口);
            groupBox2.Controls.Add(label5);
            groupBox2.Controls.Add(btn停止);
            groupBox2.Controls.Add(groupBox4);
            groupBox2.Controls.Add(txtIP);
            groupBox2.Controls.Add(chk检查账户密码);
            groupBox2.Controls.Add(label1);
            groupBox2.Controls.Add(btn启动);
            groupBox2.Location = new System.Drawing.Point(9, 10);
            groupBox2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            groupBox2.Name = "groupBox2";
            groupBox2.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            groupBox2.Size = new System.Drawing.Size(207, 246);
            groupBox2.TabIndex = 16;
            groupBox2.TabStop = false;
            groupBox2.Text = "连接服务器";
            // 
            // txt端口
            // 
            txt端口.Location = new System.Drawing.Point(74, 55);
            txt端口.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            txt端口.Name = "txt端口";
            txt端口.Size = new System.Drawing.Size(121, 23);
            txt端口.TabIndex = 30;
            txt端口.Text = "8085";
            txt端口.KeyPress += txt端口_KeyPress;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(14, 58);
            label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(56, 17);
            label5.TabIndex = 29;
            label5.Text = "端   口：";
            // 
            // btn停止
            // 
            btn停止.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            btn停止.Location = new System.Drawing.Point(119, 208);
            btn停止.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            btn停止.Name = "btn停止";
            btn停止.Size = new System.Drawing.Size(76, 28);
            btn停止.TabIndex = 28;
            btn停止.Text = "停止";
            btn停止.UseVisualStyleBackColor = true;
            btn停止.Click += btn停止_Click;
            // 
            // groupBox4
            // 
            groupBox4.Controls.Add(txt用户名);
            groupBox4.Controls.Add(label3);
            groupBox4.Controls.Add(txt密码);
            groupBox4.Controls.Add(label2);
            groupBox4.Location = new System.Drawing.Point(5, 83);
            groupBox4.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            groupBox4.Name = "groupBox4";
            groupBox4.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            groupBox4.Size = new System.Drawing.Size(196, 93);
            groupBox4.TabIndex = 27;
            groupBox4.TabStop = false;
            groupBox4.Text = "校验";
            // 
            // txt用户名
            // 
            txt用户名.Location = new System.Drawing.Point(69, 23);
            txt用户名.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            txt用户名.Name = "txt用户名";
            txt用户名.Size = new System.Drawing.Size(121, 23);
            txt用户名.TabIndex = 29;
            txt用户名.Text = "Admin";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(9, 59);
            label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(56, 17);
            label3.TabIndex = 27;
            label3.Text = "密   码：";
            // 
            // txt密码
            // 
            txt密码.Location = new System.Drawing.Point(69, 56);
            txt密码.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            txt密码.Name = "txt密码";
            txt密码.Size = new System.Drawing.Size(121, 23);
            txt密码.TabIndex = 30;
            txt密码.Text = "Admin123";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(9, 26);
            label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(56, 17);
            label2.TabIndex = 28;
            label2.Text = "用户名：";
            // 
            // txtIP
            // 
            txtIP.Location = new System.Drawing.Point(74, 23);
            txtIP.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            txtIP.Name = "txtIP";
            txtIP.Size = new System.Drawing.Size(121, 23);
            txtIP.TabIndex = 20;
            txtIP.Text = "127.0.0.1";
            // 
            // chk检查账户密码
            // 
            chk检查账户密码.AutoSize = true;
            chk检查账户密码.Location = new System.Drawing.Point(16, 182);
            chk检查账户密码.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            chk检查账户密码.Name = "chk检查账户密码";
            chk检查账户密码.Size = new System.Drawing.Size(99, 21);
            chk检查账户密码.TabIndex = 21;
            chk检查账户密码.Text = "检查账户密码";
            chk检查账户密码.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(15, 26);
            label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(31, 17);
            label1.TabIndex = 19;
            label1.Text = "IP：";
            // 
            // btn启动
            // 
            btn启动.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            btn启动.Location = new System.Drawing.Point(16, 208);
            btn启动.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            btn启动.Name = "btn启动";
            btn启动.Size = new System.Drawing.Size(76, 28);
            btn启动.TabIndex = 22;
            btn启动.Text = "启动";
            btn启动.UseVisualStyleBackColor = true;
            btn启动.Click += btn启动_Click;
            // 
            // groupBox5
            // 
            groupBox5.Controls.Add(btn退订);
            groupBox5.Controls.Add(btn订阅);
            groupBox5.Controls.Add(label4);
            groupBox5.Controls.Add(txt主题);
            groupBox5.Location = new System.Drawing.Point(9, 261);
            groupBox5.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            groupBox5.Name = "groupBox5";
            groupBox5.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            groupBox5.Size = new System.Drawing.Size(207, 111);
            groupBox5.TabIndex = 19;
            groupBox5.TabStop = false;
            groupBox5.Text = "订阅";
            // 
            // btn退订
            // 
            btn退订.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            btn退订.Location = new System.Drawing.Point(119, 72);
            btn退订.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            btn退订.Name = "btn退订";
            btn退订.Size = new System.Drawing.Size(76, 28);
            btn退订.TabIndex = 32;
            btn退订.Text = "退订";
            btn退订.UseVisualStyleBackColor = true;
            btn退订.Click += btn退订_Click;
            // 
            // btn订阅
            // 
            btn订阅.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            btn订阅.Location = new System.Drawing.Point(16, 72);
            btn订阅.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            btn订阅.Name = "btn订阅";
            btn订阅.Size = new System.Drawing.Size(76, 28);
            btn订阅.TabIndex = 31;
            btn订阅.Text = "订阅";
            btn订阅.UseVisualStyleBackColor = true;
            btn订阅.Click += btn订阅_Click;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(13, 21);
            label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(68, 17);
            label4.TabIndex = 11;
            label4.Text = "主题名称：";
            // 
            // txt主题
            // 
            txt主题.Location = new System.Drawing.Point(16, 41);
            txt主题.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            txt主题.Name = "txt主题";
            txt主题.Size = new System.Drawing.Size(179, 23);
            txt主题.TabIndex = 10;
            txt主题.Text = "TestTopic";
            // 
            // MqttNetClientWin
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(622, 382);
            Controls.Add(groupBox5);
            Controls.Add(groupBox3);
            Controls.Add(groupBox1);
            Controls.Add(groupBox2);
            Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            Name = "MqttNetClientWin";
            Text = "MQTTnet客户端";
            groupBox3.ResumeLayout(false);
            groupBox3.PerformLayout();
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            groupBox2.ResumeLayout(false);
            groupBox2.PerformLayout();
            groupBox4.ResumeLayout(false);
            groupBox4.PerformLayout();
            groupBox5.ResumeLayout(false);
            groupBox5.PerformLayout();
            ResumeLayout(false);
        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txt日志;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn发送;
        private System.Windows.Forms.TextBox txt内容;
        private System.Windows.Forms.TextBox txt当前主题;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn停止;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txt用户名;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt密码;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtIP;
        private System.Windows.Forms.CheckBox chk检查账户密码;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn启动;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txt主题;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn退订;
        private System.Windows.Forms.Button btn订阅;
        private System.Windows.Forms.TextBox txt端口;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox ckbRetained;
    }
}


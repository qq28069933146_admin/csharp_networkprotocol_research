﻿
namespace SocketWebserviceWcfWebAPITestTool.Webservice_Test
{
    partial class WebserviceFrom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            button1 = new System.Windows.Forms.Button();
            button2 = new System.Windows.Forms.Button();
            button3 = new System.Windows.Forms.Button();
            SuspendLayout();
            // 
            // button1
            // 
            button1.Location = new System.Drawing.Point(33, 12);
            button1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            button1.Name = "button1";
            button1.Size = new System.Drawing.Size(231, 38);
            button1.TabIndex = 0;
            button1.Text = "引用式调用WebService测试";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // button2
            // 
            button2.Location = new System.Drawing.Point(33, 56);
            button2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            button2.Name = "button2";
            button2.Size = new System.Drawing.Size(231, 38);
            button2.TabIndex = 1;
            button2.Text = "Http式调用WebService测试-参数组";
            button2.UseVisualStyleBackColor = true;
            button2.Click += button2_Click;
            // 
            // button3
            // 
            button3.Location = new System.Drawing.Point(33, 101);
            button3.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            button3.Name = "button3";
            button3.Size = new System.Drawing.Size(231, 38);
            button3.TabIndex = 2;
            button3.Text = "Http式调用WebService测试-拼接XML";
            button3.UseVisualStyleBackColor = true;
            button3.Click += button3_Click;
            // 
            // WebserviceFrom
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(299, 151);
            Controls.Add(button3);
            Controls.Add(button2);
            Controls.Add(button1);
            Name = "WebserviceFrom";
            Text = "      ";
            Load += WebserviceFrom_Load;
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}
﻿namespace SocketWebserviceWcfWebAPITestTool.Webservice_Test
{
    partial class Http式调用WebService_XML
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            textBox5 = new System.Windows.Forms.TextBox();
            labelb = new System.Windows.Forms.Label();
            labela = new System.Windows.Forms.Label();
            textBoxb = new System.Windows.Forms.TextBox();
            textBoxa = new System.Windows.Forms.TextBox();
            button2 = new System.Windows.Forms.Button();
            button1 = new System.Windows.Forms.Button();
            button3 = new System.Windows.Forms.Button();
            SuspendLayout();
            // 
            // textBox5
            // 
            textBox5.Location = new System.Drawing.Point(141, 12);
            textBox5.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            textBox5.Multiline = true;
            textBox5.Name = "textBox5";
            textBox5.Size = new System.Drawing.Size(332, 236);
            textBox5.TabIndex = 20;
            // 
            // labelb
            // 
            labelb.AutoSize = true;
            labelb.Location = new System.Drawing.Point(24, 166);
            labelb.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            labelb.Name = "labelb";
            labelb.Size = new System.Drawing.Size(16, 17);
            labelb.TabIndex = 19;
            labelb.Text = "b";
            // 
            // labela
            // 
            labela.AutoSize = true;
            labela.Location = new System.Drawing.Point(25, 129);
            labela.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            labela.Name = "labela";
            labela.Size = new System.Drawing.Size(15, 17);
            labela.TabIndex = 18;
            labela.Text = "a";
            // 
            // textBoxb
            // 
            textBoxb.Location = new System.Drawing.Point(43, 164);
            textBoxb.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            textBoxb.Name = "textBoxb";
            textBoxb.Size = new System.Drawing.Size(84, 23);
            textBoxb.TabIndex = 17;
            textBoxb.Text = "2";
            // 
            // textBoxa
            // 
            textBoxa.Location = new System.Drawing.Point(43, 128);
            textBoxa.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            textBoxa.Name = "textBoxa";
            textBoxa.Size = new System.Drawing.Size(84, 23);
            textBoxa.TabIndex = 16;
            textBoxa.Text = "3";
            // 
            // button2
            // 
            button2.Location = new System.Drawing.Point(11, 73);
            button2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            button2.Name = "button2";
            button2.Size = new System.Drawing.Size(115, 38);
            button2.TabIndex = 14;
            button2.Text = "调用Add方法-Post";
            button2.UseVisualStyleBackColor = true;
            button2.Click += button2_Click;
            // 
            // button1
            // 
            button1.Location = new System.Drawing.Point(11, 12);
            button1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            button1.Name = "button1";
            button1.Size = new System.Drawing.Size(115, 38);
            button1.TabIndex = 13;
            button1.Text = "欢迎-Post";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // button3
            // 
            button3.Location = new System.Drawing.Point(11, 210);
            button3.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            button3.Name = "button3";
            button3.Size = new System.Drawing.Size(115, 38);
            button3.TabIndex = 15;
            button3.Text = "调用Sub方法";
            button3.UseVisualStyleBackColor = true;
            button3.Click += button3_Click;
            // 
            // Http式调用WebService_XML
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(486, 260);
            Controls.Add(textBox5);
            Controls.Add(labelb);
            Controls.Add(labela);
            Controls.Add(textBoxb);
            Controls.Add(textBoxa);
            Controls.Add(button3);
            Controls.Add(button2);
            Controls.Add(button1);
            Name = "Http式调用WebService_XML";
            Text = "Http式调用WebService_拼接XML";
            Load += Http式调用WebService_XML_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label labelb;
        private System.Windows.Forms.Label labela;
        private System.Windows.Forms.TextBox textBoxb;
        private System.Windows.Forms.TextBox textBoxa;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
    }
}
﻿using CommonTools;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SocketWebserviceWcfWebAPITestTool.Webservice_Test
{
    public partial class Http式调用WebService_XML : Form
    {
        string url = "http://localhost:65172/WebService1.asmx";

        public Http式调用WebService_XML()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string soap_Namespace = "soap";
            string soapAction = "http://tempuri.org/HelloWorld";
            string soap_EnvelopeXml = "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"";
            string soap_HeaderXml = string.Empty;
            string soap_BodyXml = " <HelloWorld xmlns=\"http://tempuri.org/\" />";
            Encoding requestCoding = Encoding.UTF8;

            string result = RequestCom.WebServiceHttpPost(url, soapAction, soap_Namespace, soap_EnvelopeXml, soap_HeaderXml, soap_BodyXml, requestCoding);

            textBox5.Text = result;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string a = textBoxa.Text.Trim();
            string b = textBoxb.Text.Trim();

            string soapAction = "http://tempuri.org/Add";
            string soap_Namespace = "soap";
            string soap_EnvelopeXml = "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"";
            string soap_HeaderXml = string.Empty;
            string soap_BodyXml = $" <Add xmlns=\"http://tempuri.org/\"><a>{a}</a><b>{b}</b></Add>";
            Encoding requestCoding = Encoding.UTF8;

            string result = RequestCom.WebServiceHttpPost(url, soapAction, soap_Namespace, soap_EnvelopeXml, soap_HeaderXml, soap_BodyXml, requestCoding);

            textBox5.Text = result;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string a = textBoxa.Text.Trim();
            string b = textBoxb.Text.Trim();

            string soapAction = "http://tempuri.org/Sub";
            string soap_Namespace = "soap";
            string soap_EnvelopeXml = "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"";
            string soap_HeaderXml = string.Empty;
            string soap_BodyXml = $" <Sub xmlns=\"http://tempuri.org/\"><tc1><num1>{a}</num1><num2>{b}</num2></tc1></Sub>";
            Encoding requestCoding = Encoding.UTF8;

            string result = RequestCom.WebServiceHttpPost(url, soapAction, soap_Namespace, soap_EnvelopeXml, soap_HeaderXml, soap_BodyXml, requestCoding);

            textBox5.Text = result;
        }

        private void Http式调用WebService_XML_Load(object sender, EventArgs e)
        {

        }
    }
}

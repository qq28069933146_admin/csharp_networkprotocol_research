﻿
namespace WebserviceWcfWebAPITestTool
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            btn常规Get与Post = new System.Windows.Forms.Button();
            button3 = new System.Windows.Forms.Button();
            button4 = new System.Windows.Forms.Button();
            button5 = new System.Windows.Forms.Button();
            label1 = new System.Windows.Forms.Label();
            SuspendLayout();
            // 
            // btn常规Get与Post
            // 
            btn常规Get与Post.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            btn常规Get与Post.Location = new System.Drawing.Point(11, 10);
            btn常规Get与Post.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            btn常规Get与Post.Name = "btn常规Get与Post";
            btn常规Get与Post.Size = new System.Drawing.Size(177, 42);
            btn常规Get与Post.TabIndex = 0;
            btn常规Get与Post.Text = "Http调用";
            btn常规Get与Post.UseVisualStyleBackColor = true;
            btn常规Get与Post.Click += btn常规Get与Post_Click;
            // 
            // button3
            // 
            button3.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            button3.Location = new System.Drawing.Point(11, 63);
            button3.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            button3.Name = "button3";
            button3.Size = new System.Drawing.Size(177, 42);
            button3.TabIndex = 2;
            button3.Text = "WCF测试";
            button3.UseVisualStyleBackColor = true;
            button3.Click += button3_Click;
            // 
            // button4
            // 
            button4.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            button4.Location = new System.Drawing.Point(192, 63);
            button4.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            button4.Name = "button4";
            button4.Size = new System.Drawing.Size(177, 42);
            button4.TabIndex = 3;
            button4.Text = "WebSocket测试";
            button4.UseVisualStyleBackColor = true;
            button4.Click += button4_Click;
            // 
            // button5
            // 
            button5.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            button5.Location = new System.Drawing.Point(192, 10);
            button5.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            button5.Name = "button5";
            button5.Size = new System.Drawing.Size(177, 42);
            button5.TabIndex = 4;
            button5.Text = "Webservice测试";
            button5.UseVisualStyleBackColor = true;
            button5.Click += button5_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(118, 244);
            label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(128, 17);
            label1.TabIndex = 5;
            label1.Text = "不要学我用中文编码呦";
            // 
            // MainForm
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(378, 266);
            Controls.Add(label1);
            Controls.Add(button5);
            Controls.Add(button4);
            Controls.Add(button3);
            Controls.Add(btn常规Get与Post);
            Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            Name = "MainForm";
            Text = "WebAPI技术测试工具-执笔小白";
            Load += MainForm_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Button btn常规Get与Post;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label1;
    }
}


﻿namespace Socket_TCPAndUDPWin
{
    partial class UDPWin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            txtPort = new System.Windows.Forms.TextBox();
            btnSendMsg = new System.Windows.Forms.Button();
            txtMsg = new System.Windows.Forms.TextBox();
            label2 = new System.Windows.Forms.Label();
            txtIP = new System.Windows.Forms.TextBox();
            label1 = new System.Windows.Forms.Label();
            btnOpenServer = new System.Windows.Forms.Button();
            panel1 = new System.Windows.Forms.Panel();
            txtInfo = new System.Windows.Forms.TextBox();
            panel1.SuspendLayout();
            SuspendLayout();
            // 
            // txtPort
            // 
            txtPort.Location = new System.Drawing.Point(261, 11);
            txtPort.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            txtPort.Name = "txtPort";
            txtPort.Size = new System.Drawing.Size(78, 23);
            txtPort.TabIndex = 44;
            txtPort.Text = "8081";
            // 
            // btnSendMsg
            // 
            btnSendMsg.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            btnSendMsg.Location = new System.Drawing.Point(401, 20);
            btnSendMsg.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            btnSendMsg.Name = "btnSendMsg";
            btnSendMsg.Size = new System.Drawing.Size(81, 40);
            btnSendMsg.TabIndex = 41;
            btnSendMsg.Text = "发送";
            btnSendMsg.UseVisualStyleBackColor = true;
            btnSendMsg.Click += btnSendMsg_Click;
            // 
            // txtMsg
            // 
            txtMsg.Location = new System.Drawing.Point(69, 45);
            txtMsg.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            txtMsg.Name = "txtMsg";
            txtMsg.Size = new System.Drawing.Size(313, 23);
            txtMsg.TabIndex = 40;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(9, 48);
            label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(44, 17);
            label2.TabIndex = 39;
            label2.Text = "消息：";
            // 
            // txtIP
            // 
            txtIP.Location = new System.Drawing.Point(69, 11);
            txtIP.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            txtIP.Name = "txtIP";
            txtIP.Size = new System.Drawing.Size(188, 23);
            txtIP.TabIndex = 38;
            txtIP.Text = "127.0.0.1";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(9, 14);
            label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(56, 17);
            label1.TabIndex = 37;
            label1.Text = "监听端口";
            // 
            // btnOpenServer
            // 
            btnOpenServer.Location = new System.Drawing.Point(20, 16);
            btnOpenServer.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            btnOpenServer.Name = "btnOpenServer";
            btnOpenServer.Size = new System.Drawing.Size(73, 30);
            btnOpenServer.TabIndex = 10;
            btnOpenServer.Text = "开启监听";
            btnOpenServer.UseVisualStyleBackColor = true;
            btnOpenServer.Click += btnOpenServer_Click;
            // 
            // panel1
            // 
            panel1.Controls.Add(btnOpenServer);
            panel1.Location = new System.Drawing.Point(500, 9);
            panel1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            panel1.Name = "panel1";
            panel1.Size = new System.Drawing.Size(110, 62);
            panel1.TabIndex = 42;
            // 
            // txtInfo
            // 
            txtInfo.Location = new System.Drawing.Point(9, 77);
            txtInfo.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            txtInfo.Multiline = true;
            txtInfo.Name = "txtInfo";
            txtInfo.Size = new System.Drawing.Size(604, 296);
            txtInfo.TabIndex = 43;
            // 
            // UDPWin
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(622, 382);
            Controls.Add(txtPort);
            Controls.Add(btnSendMsg);
            Controls.Add(txtMsg);
            Controls.Add(label2);
            Controls.Add(txtIP);
            Controls.Add(label1);
            Controls.Add(panel1);
            Controls.Add(txtInfo);
            Name = "UDPWin";
            Text = "UDPClient示例";
            Load += UDPWin_Load;
            panel1.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.Button btnSendMsg;
        private System.Windows.Forms.TextBox txtMsg;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtIP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnOpenServer;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtInfo;
    }
}
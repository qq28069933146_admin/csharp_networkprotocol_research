﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TCPAndUDPWin;

namespace Socket_TCPAndUDPWin
{
    public partial class UDPWin : Form
    {
        #region 变量
        /// <summary>
        /// UDP帮助类
        /// </summary>
        UDPHelper _UDPHelper = new UDPHelper();
        #endregion 变量

        public UDPWin()
        {
            InitializeComponent();
        }

        private void UDPWin_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 开启/关闭侦听-控制线程
        /// </summary>
        private void btnOpenServer_Click(object sender, EventArgs e)
        {
            try
            {
                #region Net5
                if (btnOpenServer.Text == "开启监听")  // 开启监听
                {
                    // 检查地址
                    if (!CheckServerUrl())
                    {
                        return;
                    }

                    string iPStr = txtIP.Text.Trim();  // IP
                    int port = int.Parse(txtPort.Text);  // 端口

                    Action<ResultData_TCP>? callback = UdpListener_ResultData_UdpHandle;

                    _UDPHelper.BeginReceive(iPStr, port, callback);

                    btnOpenServer.Text = "关闭监听";
                    btnOpenServer.BackColor = Color.FromArgb(128, 255, 128);
                }
                else  // 关闭监听
                {
                    _UDPHelper.EndReceive();

                    btnOpenServer.Text = "开启监听";
                    btnOpenServer.BackColor = Color.FromArgb(255, 128, 128);
                }
                #endregion Net5
            }
            catch (Exception ex)
            {
                btnOpenServer.Text = "开启监听";
                btnOpenServer.BackColor = Color.FromArgb(255, 128, 128);

                ShowLog($"开启/关闭监听失败，错误信息: {ex.Message}");
            }
        }

        /// <summary>
        /// TdpListener接收信息后的处理
        /// </summary>
        private void UdpListener_ResultData_UdpHandle(ResultData_TCP resultData_TCP)
        {
            this.Invoke(new Action(() =>
            {
                txtInfo.Text += string.Concat("\r\n", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), "->", " ResultCode：", resultData_TCP.ResultCode, "；ResultMsg：", resultData_TCP.ResultMsg, "接收信息：", resultData_TCP.ResultObject1);
            }));
        }

        /// <summary>
        /// 发送
        /// </summary>
        private void btnSendMsg_Click(object sender, EventArgs e)
        {
            try
            {
                // 检查地址
                if (!CheckServerUrl())
                {
                    return;
                }

                string iP = txtIP.Text;               // IP
                int port = int.Parse(txtPort.Text);   // 端口
                string sendMsg = txtMsg.Text.Trim();  // 信息
                if (string.IsNullOrEmpty(sendMsg))
                {
                    ShowLog("发送信息不可为空！");
                    return;
                }

                ResultData_TCP resultData_TCP = _UDPHelper.ConnectAndSed(iP, port, sendMsg);  // 发送信息

                ShowLog(string.Concat("发送信息:内容：", sendMsg, "；结果：", resultData_TCP.ResultCode, "_", resultData_TCP.ResultMsg));
            }
            catch (Exception ex)
            {
                ShowLog(string.Concat("发送信息错误，错误信息：", ex.Message));
            }
        }

        /// <summary>
        /// 显示信息
        /// </summary>
        /// <param name="msg">信息</param>
        public void ShowLog(string msg)
        {
            txtInfo.Text += string.Concat("\r\n", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), "->", msg);
        }

        /// <summary>
        /// 检查连接是否正常
        /// </summary>
        /// <returns>true为通过</returns>
        private bool CheckServerUrl()
        {
            if (string.IsNullOrEmpty(txtIP.Text))  // 
            {
                ShowLog("发送信息不可为空！");
                return false;
            }
            if (string.IsNullOrEmpty(txtPort.Text))  // 
            {
                ShowLog("端口不可为空！");
                return false;
            }
            return true;
        }
    }
}

# CSharp_NetworkProtocol_Research

#### 介绍
C#_网络协议研究（原github Net5-Web-APIs-Demo与Net5-Web-APIs-TestTools项目）

研究的协议包括TCP、UDP、Socket、Webservice、WCF、WebAPI、MQTT


#### 软件架构
博文地址：<a href="https://www.cnblogs.com/qq2806933146xiaobai/p/15378767.html">C#-Socket_TCP、Socket_UDP、WebSocket、WebService、WCF、WebAPI、MQTT的基础</a>
视频演示：<a href="">https://www.bilibili.com/video/BV1vM4y1B7jN/?vd_source=3f52ff89411950a529a4b03056f38f16</a>
